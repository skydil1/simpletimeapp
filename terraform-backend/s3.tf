resource "aws_s3_bucket" "state_s3" {
  bucket = "terraform-state-simple-time"

  tags = {
    Name = "State Bucket"
  }
}

resource "aws_s3_bucket_versioning" "state_s3" {
  bucket = aws_s3_bucket.state_s3.id
  versioning_configuration {
    status = "Enabled"
  }
}