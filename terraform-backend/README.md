# Terraform Remote State

This terraform code creates the S3 bucket and Dynamodb table required for configuring terraform remote state.

### Advantages of Remote State:

1. Enhanced Security: By storing state on a remote server, sensitive information is better protected. Remote storage solutions like S3 offer additional security layers, such as making the bucket private and implementing limited access controls. This mitigates the risk of unauthorized access to critical data.

2. Auditing Capabilities: Enabling logging on remote storage allows for better auditing. Any unauthorized access attempts or suspicious activities can be easily identified and investigated, enhancing the overall security posture of the infrastructure.

3. Collaborative Workflows: Utilizing remote storage facilitates seamless collaboration among team members. The state file can be easily shared and accessed by multiple team members, enabling efficient collaboration on infrastructure management tasks without compromising security or data integrity.

### Resources 
This terraform code creates S3 bucket terraform-state-simple-time for storing the Terraform state file. Versioning and Default encryption is enabled for the bucket.

A DyanmoDB tableterraform-state-lock is used for locking your state for all operations that could write state. This prevents others from acquiring the lock and potentially corrupting your state.”

The AWS credentials should be exported as environmental variables in the terminal. 

```
export AWS_SECRET_ACCESS_KEY="secret-key" && export AWS_ACCESS_KEY_ID="access-key
```

To create the resouces
```
cd terraform-backend/
terraform init
terraform apply
```