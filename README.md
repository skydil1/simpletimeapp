# SimpleTimeApp

This repository contains components for building, deploying the SimpleTimeApp application, as well as infrastructure setup for the app.

## Repository Structure

```
simpletimeapp/
│
├── app
├── deployment
├── terraform-backend
├── terraform
├── .gitlab-ci.yml
├── README.md

```

## Step-by-Step Deployment Process

It's crucial to follow the step-by-step process outlined below to build and deploy the app in an EKS cluster.

### App

The `app` folder contains the necessary components to build and push the image to Docker Hub. It also includes a pipeline for CI/CD.

### Terraform Backend

The `terraform-backend` folder holds components to set up the backend for Terraform, such as S3 and DynamoDB table. This step should be completed before running the pipeline in the `terraform` folder.

### Terraform

The `terraform` directory contains Terraform code to set up a VPC and an EKS cluster.

### Deployment

In the `deployment` directory, you'll find the Terraform code to deploy the app as a deployment and service to the EKS cluster. Additionally, setup for Grafana is included here.

## GitlabCI

The `.gitlab-ci.yml` file includes the pipeline configurations for the `app` and `terraform` directories.

