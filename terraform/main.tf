
terraform {
  backend "s3" {
    bucket         = "terraform-state-skydil-simple-time"
    key            = "terraform.tfstate"
    region         = "ap-south-1"
    dynamodb_table = "terraform-state-lock"
    encrypt        = true
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Environment = "${var.env}"
      Project     = "${var.project}"
      ManagedBy   = "Terraform"
      Costcenter  = "Platform"
    }
  }
}