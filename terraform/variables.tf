variable "region" {
  type        = string
  description = "AWS Region"
  default     = "ap-south-1"
}
variable "env" {
  type        = string
  description = "Product Env"
  default     = "test"
}

variable "project" {
  type        = string
  description = "Name of Project"
  default     = "simpletimeapp"
}

variable "simpletime_vpc_azs" {
  type        = list(string)
  description = "AWS avaliablity zones"
  default     = ["ap-south-1a", "ap-south-1b"]
}

variable "simpletime_vpc_cidr" {
  type        = string
  description = "CIDR block for the VPC"
  default     = "10.10.60.0/22"
}

variable "simpletime_vpc_public_subnets" {
  description = "CIDR blocks for the public subnets"
  type        = list(string)
  default     = ["10.10.60.0/24", "10.10.61.0/24"]
}

variable "simpletime_vpc_private_subnets" {
  description = "CIDR blocks for the private subnets"
  type        = list(string)
  default     = ["10.10.62.0/24", "10.10.63.0/24"]
}

variable "eks_cluster" {
  type        = string
  description = "EKS cluster Name"
  default     = "eks-simpletime"
}