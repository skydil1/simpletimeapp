### Resources 
This terraform code creates VPS with two Private subnets and two public subnets. This will also create an EKS cluster with two worker nodes.

The AWS credentials should be exported as environmental variables in the terminal. 

```
export AWS_SECRET_ACCESS_KEY="secret-key" && export AWS_ACCESS_KEY_ID="access-key
```

To create the resouces
```
cd terraform/
terraform init
terraform plan
terraform apply
```
### CI/CD Pipeline

A GitLab CI/CD pipeline is set up with terraform plan and apply stages. The pipeline configuration can be found in the `.gitlab-ci.yml` file.

### GitlabCI variables
```
AWS_SECRET_ACCESS_KEY
AWS_ACCESS_KEY_ID
```
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.12.1 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.27.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_eks"></a> [eks](#module\_eks) | terraform-aws-modules/eks/aws | ~> 20.0 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 5.6.0 |


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_eks_cluster"></a> [eks\_cluster](#input\_eks\_cluster) | EKS cluster Name | `string` | `"eks-simpletime"` | no |
| <a name="input_env"></a> [env](#input\_env) | Product Env | `string` | `"test"` | no |
| <a name="input_project"></a> [project](#input\_project) | Name of Project | `string` | `"simpletimeapp"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS Region | `string` | `"ap-south-1"` | no |
| <a name="input_simpletime_vpc_azs"></a> [simpletime\_vpc\_azs](#input\_simpletime\_vpc\_azs) | AWS avaliablity zones | `list(string)` | <pre>[<br>  "ap-south-1a",<br>  "ap-south-1b"<br>]</pre> | no |
| <a name="input_simpletime_vpc_cidr"></a> [simpletime\_vpc\_cidr](#input\_simpletime\_vpc\_cidr) | CIDR block for the VPC | `string` | `"10.10.60.0/22"` | no |
| <a name="input_simpletime_vpc_private_subnets"></a> [simpletime\_vpc\_private\_subnets](#input\_simpletime\_vpc\_private\_subnets) | CIDR blocks for the private subnets | `list(string)` | <pre>[<br>  "10.10.62.0/24",<br>  "10.10.63.0/24"<br>]</pre> | no |
| <a name="input_simpletime_vpc_public_subnets"></a> [simpletime\_vpc\_public\_subnets](#input\_simpletime\_vpc\_public\_subnets) | CIDR blocks for the public subnets | `list(string)` | <pre>[<br>  "10.10.60.0/24",<br>  "10.10.61.0/24"<br>]</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->