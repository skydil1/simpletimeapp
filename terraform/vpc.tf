module "vpc" {
  source          = "terraform-aws-modules/vpc/aws"
  version         = "5.6.0"
  name            = "simpletime-testing-vpc"
  cidr            = var.simpletime_vpc_cidr
  azs             = var.simpletime_vpc_azs
  private_subnets = var.simpletime_vpc_private_subnets
  public_subnets  = var.simpletime_vpc_public_subnets

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_vpn_gateway   = false
  enable_dns_hostnames = true

  tags = {
    environment = "test"
    name        = "simpletime-testing-vpc"
  }

  public_subnet_tags = {
    Type                                       = "public"
    SubnetType                                 = "Utility"
    "kubernetes.io/cluster/${var.eks_cluster}" = "shared"
    "kubernetes.io/role/elb"                   = "1"
  }

  private_subnet_tags = {
    Type                                       = "private"
    SubnetType                                 = "Private"
    "kubernetes.io/cluster/${var.eks_cluster}" = "shared"
    "kubernetes.io/role/internal-elb"          = "1"
  }
}