resource "kubernetes_namespace" "simple_time_service" {
  metadata {
    annotations = {
      name = "simple-time-service"
    }

    labels = {
      app = "simple-time-service"
    }

    name = "simple-time-service"
  }
}
resource "kubernetes_deployment" "simple_time_service_deployment" {
  metadata {
    name = "simple-time-service-deployment"
    namespace = "simple-time-service"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "simple-time-service"
      }
    }

    template {
      metadata {
        labels = {
          app = "simple-time-service"
        }
      }

      spec {
        container {
          name  = "simple-time-service"
          image = "aramesh166/simple-time-service:latest"
          port {
            container_port = 5000
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }

        }
        container {
          name  = "fluentd-sidecar"
          image = "fluentd:latest"
          port {
            container_port = 24224
            name           = "fluentd"
          }
          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "simple_time_service_service" {
  metadata {
    name = "simple-time-service"
    namespace = "simple-time-service"
  }
  spec {
    selector = {
      app = "simple-time-service"
    }
    port {
      protocol    = "TCP"
      port        = 80
      target_port = 5000
    }
    type = "ClusterIP"
  }
}
