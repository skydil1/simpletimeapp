### Resources 
This terraform code creates a deployment, service and namespace for the app 'simpletimeapp' which was build in ../app directory. This also install a helm chart for Grafana.

The AWS credentials should be exported as environmental variables in the terminal. 

```
export AWS_SECRET_ACCESS_KEY="secret-key" && export AWS_ACCESS_KEY_ID="access-key
```

To create the resouces
```
cd terraform/
terraform init
terraform plan
terraform apply
```

Use below command to login to cluster and see  it the pods are running fine. Replace aws-profile-name name with your profile and eks-cluster-name with the EKS clustername.

```
aws eks update-kubeconfig --name eks-cluster-name --profile aws-profile-name
```