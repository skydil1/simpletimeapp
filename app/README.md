# Simple Time Service

This is a Python-based web application that displays the current date and time along with the IP address from which it is accessed.

## Features

- Displays the current date and time.
- Shows the IP address of the visitor.

## Usage

To run the application locally, make sure you have Python installed. Then, follow these steps:

1. Clone this repository:

   ```bash
   git@gitlab.com:skydil1/simpletimeapp.git
   ```

2. Navigate to the project directory:

   ```bash
   cd app
   ```

3. Install the dependencies:

   ```bash
   pip install -r requirements.txt
   ```

4. Run the application:

   ```bash
   python main.py
   ```

## Docker

Alternatively, you can run the application using Docker. A Dockerfile is provided in the repository.
   ```bash
   - cd app
    - docker build -t aramesh166/simple-time-service:latest .
    - docker push aramesh166/simple-time-service:latest
    - docker run -d -p 5000:5000 aramesh166/simple-time-service:latest
   ```
The application will be accessible at `http://localhost:5000`.
### Build Docker Image

To build the Docker image locally, run the following command in the project directory:

```bash
docker build -t simple-time-service:latest .
```

### Docker Hub

The Docker image is also available on Docker Hub at [aramesh166/simple-time-service](https://hub.docker.com/r/aramesh166/simple-time-service). You can pull the image directly from Docker Hub:

```bash
docker pull aramesh166/simple-time-service:latest
```

### CI/CD Pipeline

A GitLab CI/CD pipeline is set up to automatically build and push the Docker image to Docker Hub whenever changes are pushed to the repository. The pipeline configuration can be found in the `.gitlab-ci.yml` file.

### GitlabCI variables
```
DOCKER_REGISTRY
DOCKER_REGISTRY_USER
DOCKER_REGISTRY_PASS
```
## Good to Know
To get the public IP address of the client directly from the request object in Flask, you can't rely solely on the request object. This is because Flask itself does not provide a direct way to retrieve the public IP address of the client making the request. The request.remote_addr attribute typically provides the IP address of the client as seen by the server, which might be a private IP address if the client is behind a proxy or NAT.

To get the public IP address of the client, you would typically need to use a service or API that can determine the public IP address from which the request is originating. This is why using an external service like httpbin.org or a similar service is often necessary.


## Kubernets Deployment file
This repository also includes manifest files for deploying the application in Kubernetes environments. The manifests create a namespace, deployment, and a service with ClusterIP. The ClusterIP can be exposed and utilized with the assistance of Ingress, depending on the environment. The Ingress configuration is not included in this repository.

```bash
kubectl apply -f microservice.yml
```